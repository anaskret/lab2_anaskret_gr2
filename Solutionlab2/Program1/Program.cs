using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Program1
{
    class Program
    {

        public class Student
        {
            public string firstName { get; set; }
            public string lastName { get; set; }
            public int numTel { get; set; }
        }
        static void Main(string[] args)
        {
            bool petla = true;
            var students = new List<Student>();

            while (petla)
            {
                Console.WriteLine("");
                Console.WriteLine("******MENU******");
                Console.WriteLine("1. Dodaj ucznia");
                Console.WriteLine("2. Usun ucznia");
                Console.WriteLine("3. Wyswietl liste uczniow");
                Console.WriteLine("4. Koniec");

                Console.WriteLine("Wybierz jedna z opcji: ");
                int x;
                x = Convert.ToInt32(Console.ReadLine());

                switch (x)
                {
                    case 1:
                        string firstName;
                        Console.WriteLine("Imie ucznia (nie moze zawierac cyfr): ");
                        do
                        {
                            firstName = Console.ReadLine();
                        } while (firstName.Any(char.IsDigit));

                        string lastName;
                        Console.WriteLine("Nazwisko ucznia (nie moze zawierac cyfr): ");
                        do
                        {
                            lastName = Console.ReadLine();
                        } while (lastName.Any(char.IsDigit));

                        string Tel;
                        int numTel;
                        Console.WriteLine("Numer telefonu ucznia: ");
                        while (true)
                        {
                            Tel = Console.ReadLine();
                            if (Tel.Any(char.IsLetter)) 
                            {
                                Console.WriteLine("Numer nie mo�e zawierac liter");
                                continue;
                            }
                            numTel=Convert.ToInt32(Tel);
                            if (numTel < 100000000 || numTel > 999999999) Console.WriteLine("Podaj 9 cyfrowy numer");
                            else break;
                        }

                        var student1 = new Student();
                        student1.firstName = firstName;
                        student1.lastName = lastName;
                        student1.numTel = numTel;
                        students.Add(student1);
                        Console.WriteLine("");
                        break;
                    case 2:
                        int i = 0;
                        foreach (var item in students)
                        {
                            Console.WriteLine
                                ($"{i}) First name: {item.firstName}, Last name: {item.lastName}, Numer telefonu: {item.numTel}");
                            i++;
                        }
                        Console.WriteLine("Wybierz ktorego ucznia chcesz usunac: ");
                        int usun;
                        string usunChar;
                        while(true)
                        {
                            usunChar = Console.ReadLine();
                            if(usunChar.Any(char.IsLetter))
                            {
                                Console.WriteLine("Wpisz odpowiedni numer");
                                continue;
                            }
                            usun = Convert.ToInt32(usunChar);
                            if (usun < 0 || usun > i) Console.WriteLine("Wpisz prawidlowy indeks ucznia do usuniecia");
                            else break;
                        }
                        students.RemoveAt(usun);
                        Console.WriteLine("");
                        break;
                    case 3:
                        Console.WriteLine("Jak posortowac uczniow? ");
                        Console.WriteLine("a) Sortujac po nazwisku ");
                        Console.WriteLine("b) Sortujac po imieniu ");
                        Console.WriteLine("c) Sortujac po numerze telefonu ");
                        string wybor = Console.ReadLine();
                        while (true)
                        {
                            if (wybor == "a")
                            {
                                foreach (var item in students.OrderBy(s => s.lastName))
                                {
                                    Console.WriteLine
                                        ($"First name: {item.firstName}, Last name: {item.lastName}, Numer telefonu: {item.numTel}");
                                }
                                break;

                            }
                            else if (wybor == "b")
                            {
                                foreach (var item in students.OrderBy(s => s.firstName))
                                {
                                    Console.WriteLine
                                        ($"First name: {item.firstName}, Last name: {item.lastName}, Numer telefonu: {item.numTel}");
                                }
                                break;
                            }
                            else if (wybor == "c")
                            {
                                foreach (var item in students.OrderBy(s => s.numTel))
                                {
                                    Console.WriteLine
                                        ($"First name: {item.firstName}, Last name: {item.lastName}, Numer telefonu: {item.numTel}");
                                }
                                break;

                            }
                            else Console.WriteLine("Wybierz 'a', 'b' lub 'c'");
                        }
                        Console.WriteLine("");
                        break;
                    case 4:
                        petla = false;
                        break;
                    default:
                        Console.WriteLine("Wybierz prawidlowa opcje!");
                        break;
                }

            }

        }
    }
}

/*
 * public class Studenet
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    internal class Program
    {
        private static void Main(string[] args)
        {
            var students = new List<Studenet>();
            while (true) 
            {
                Console.WriteLine("Student first name: ");
                string firstName = Console.ReadLine();
                Console.WriteLine("Student last name: ");
                string lastName = Console.ReadLine();

                var student = new Studenet();
                student.FirstName = firstName;
                student.LastName = lastName;
                students.Add(student);

                Console.WriteLine("Exit: 0, Continue: 1 ");
                int option;
                int.TryParse(Console.ReadLine(), out option);

                if (option == 0)
                    break;
            }

            Console.WriteLine("Active students:");
            foreach (var item in students.Order(s=>s.LastName))
            {
                Console.WriteLine
                    ($"First name: {item.FirstName}, Last name: {item.LastName}");
            }
        }
    }
*/
